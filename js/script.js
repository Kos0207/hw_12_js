document.addEventListener("keydown", (e) => {
  const key = e.key.toLocaleLowerCase();
  // console.log(key);
  const buttons = document.querySelectorAll(".btn");
  buttons.forEach(button => {
    let value = button.textContent.toLocaleLowerCase()
    // console.log(value);
    if (value === key) {
      button.classList.add("active");
    } else {
      button.classList.remove("active");
    }
  });
})
 



